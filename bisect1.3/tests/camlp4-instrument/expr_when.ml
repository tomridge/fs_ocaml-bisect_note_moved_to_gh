let f x y = function
  | 0 when x = y -> 1
  | 1 when x < y -> 2
  | 2 when x > y -> 3
  | _ -> 0
