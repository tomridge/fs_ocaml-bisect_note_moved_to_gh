type token =
  | CLOSING_PARENT
  | OPENING_PARENT
  | COMMA
  | PLUS
  | MINUS
  | MULTIPLY
  | DIVIDE
  | EOF
  | IDENT of (string)
  | FILE of (string)
  | FILES of (string)
  | INTEGER of (int)

val start :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> CombineAST.expr
