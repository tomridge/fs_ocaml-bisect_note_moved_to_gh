type token =
  | CLOSING_PARENT
  | OPENING_PARENT
  | COMMA
  | PLUS
  | MINUS
  | MULTIPLY
  | DIVIDE
  | EOF
  | IDENT of (string)
  | FILE of (string)
  | FILES of (string)
  | INTEGER of (int)

open Parsing;;
let _ = parse_error;;
# 20 "src/report/combineParser.mly"

type error =
  | Invalid_expression

let string_of_error = function
  | Invalid_expression -> "invalid expression"

let fail error =
  let open Lexing in
  let pos = Parsing.symbol_start_pos () in
  let msg =
    Printf.sprintf "character %d: %s"
      pos.pos_cnum
      (string_of_error error) in
  failwith msg

# 35 "src/report/combineParser.ml"
let yytransl_const = [|
  257 (* CLOSING_PARENT *);
  258 (* OPENING_PARENT *);
  259 (* COMMA *);
  260 (* PLUS *);
  261 (* MINUS *);
  262 (* MULTIPLY *);
  263 (* DIVIDE *);
    0 (* EOF *);
    0|]

let yytransl_block = [|
  264 (* IDENT *);
  265 (* FILE *);
  266 (* FILES *);
  267 (* INTEGER *);
    0|]

let yylhs = "\255\255\
\001\000\002\000\002\000\002\000\002\000\002\000\002\000\002\000\
\002\000\002\000\002\000\003\000\003\000\000\000"

let yylen = "\002\000\
\002\000\003\000\003\000\003\000\003\000\003\000\004\000\001\000\
\001\000\001\000\001\000\001\000\003\000\002\000"

let yydefred = "\000\000\
\000\000\000\000\011\000\000\000\000\000\008\000\009\000\010\000\
\014\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\001\000\006\000\000\000\000\000\000\000\000\000\004\000\005\000\
\007\000\000\000\000\000"

let yydgoto = "\002\000\
\009\000\010\000\020\000"

let yysindex = "\255\255\
\021\255\000\000\000\000\021\255\017\255\000\000\000\000\000\000\
\000\000\001\000\032\255\021\255\021\255\021\255\021\255\021\255\
\000\000\000\000\254\254\006\255\018\255\018\255\000\000\000\000\
\000\000\021\255\254\254"

let yyrindex = "\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\009\255\000\000\008\000\013\000\000\000\000\000\
\000\000\000\000\019\255"

let yygindex = "\000\000\
\000\000\002\000\000\000"

let yytablesize = 274
let yytable = "\001\000\
\017\000\013\000\014\000\015\000\016\000\011\000\025\000\002\000\
\026\000\012\000\000\000\012\000\003\000\019\000\021\000\022\000\
\023\000\024\000\012\000\013\000\003\000\013\000\004\000\015\000\
\016\000\000\000\000\000\027\000\005\000\006\000\007\000\008\000\
\018\000\000\000\000\000\013\000\014\000\015\000\016\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\013\000\014\000\015\000\016\000\
\002\000\000\000\002\000\002\000\002\000\003\000\000\000\003\000\
\003\000\003\000"

let yycheck = "\001\000\
\000\000\004\001\005\001\006\001\007\001\004\000\001\001\000\000\
\003\001\001\001\255\255\003\001\000\000\012\000\013\000\014\000\
\015\000\016\000\002\001\001\001\000\001\003\001\002\001\006\001\
\007\001\255\255\255\255\026\000\008\001\009\001\010\001\011\001\
\001\001\255\255\255\255\004\001\005\001\006\001\007\001\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\004\001\005\001\006\001\007\001\
\001\001\255\255\003\001\004\001\005\001\001\001\255\255\003\001\
\004\001\005\001"

let yynames_const = "\
  CLOSING_PARENT\000\
  OPENING_PARENT\000\
  COMMA\000\
  PLUS\000\
  MINUS\000\
  MULTIPLY\000\
  DIVIDE\000\
  EOF\000\
  "

let yynames_block = "\
  IDENT\000\
  FILE\000\
  FILES\000\
  INTEGER\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 54 "src/report/combineParser.mly"
                                                 ( _1 )
# 186 "src/report/combineParser.ml"
               : CombineAST.expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 56 "src/report/combineParser.mly"
                                                 ( CombineAST.(Binop (Plus, _1, _3)) )
# 194 "src/report/combineParser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 57 "src/report/combineParser.mly"
                                                 ( CombineAST.(Binop (Minus, _1, _3)) )
# 202 "src/report/combineParser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 58 "src/report/combineParser.mly"
                                                 ( CombineAST.(Binop (Multiply, _1, _3)) )
# 210 "src/report/combineParser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 59 "src/report/combineParser.mly"
                                                 ( CombineAST.(Binop (Divide, _1, _3)) )
# 218 "src/report/combineParser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 60 "src/report/combineParser.mly"
                                                 ( _2 )
# 225 "src/report/combineParser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 3 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 1 : 'expr_list) in
    Obj.repr(
# 61 "src/report/combineParser.mly"
                                                 ( CombineAST.Function (_1, List.rev _3) )
# 233 "src/report/combineParser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 62 "src/report/combineParser.mly"
                                                 ( CombineAST.File _1 )
# 240 "src/report/combineParser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 63 "src/report/combineParser.mly"
                                                 ( CombineAST.Files _1 )
# 247 "src/report/combineParser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : int) in
    Obj.repr(
# 64 "src/report/combineParser.mly"
                                                 ( CombineAST.Integer _1 )
# 254 "src/report/combineParser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    Obj.repr(
# 65 "src/report/combineParser.mly"
                                                 ( fail Invalid_expression )
# 260 "src/report/combineParser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 67 "src/report/combineParser.mly"
                                                 ( [_1] )
# 267 "src/report/combineParser.ml"
               : 'expr_list))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr_list) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 68 "src/report/combineParser.mly"
                                                 ( _3 :: _1 )
# 275 "src/report/combineParser.ml"
               : 'expr_list))
(* Entry start *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let start (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf : CombineAST.expr)
;;
