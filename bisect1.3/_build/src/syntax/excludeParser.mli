type token =
  | CLOSING_BRACKET
  | OPENING_BRACKET
  | SEMICOLON
  | FILE
  | NAME
  | REGEXP
  | EOF
  | STRING of (string)

val file :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> Exclude.file list
