type token =
  | CLOSING_BRACKET
  | OPENING_BRACKET
  | SEMICOLON
  | FILE
  | NAME
  | REGEXP
  | EOF
  | STRING of (string)

open Parsing;;
let _ = parse_error;;
# 20 "src/syntax/excludeParser.mly"

type error =
  | Invalid_file_contents
  | Invalid_file_declaration
  | Invalid_exclusion
  | Invalid_regular_expression of string

let string_of_error = function
  | Invalid_file_contents -> "invalid file contents"
  | Invalid_file_declaration -> "invalid file declaration"
  | Invalid_exclusion -> "invalid exclusion"
  | Invalid_regular_expression re -> Printf.sprintf "invalid regular expression %S" re

let fail error =
  let pos = Parsing.symbol_start_pos () in
  let line = pos.Lexing.pos_lnum in
  raise (Exclude.Exception (line, string_of_error error))

# 33 "src/syntax/excludeParser.ml"
let yytransl_const = [|
  257 (* CLOSING_BRACKET *);
  258 (* OPENING_BRACKET *);
  259 (* SEMICOLON *);
  260 (* FILE *);
  261 (* NAME *);
  262 (* REGEXP *);
    0 (* EOF *);
    0|]

let yytransl_block = [|
  263 (* STRING *);
    0|]

let yylhs = "\255\255\
\001\000\001\000\002\000\002\000\003\000\003\000\004\000\004\000\
\006\000\006\000\006\000\005\000\005\000\000\000"

let yylen = "\002\000\
\002\000\001\000\000\000\002\000\006\000\002\000\000\000\002\000\
\003\000\003\000\001\000\000\000\001\000\002\000"

let yydefred = "\000\000\
\000\000\000\000\002\000\014\000\000\000\000\000\001\000\004\000\
\006\000\000\000\007\000\000\000\011\000\000\000\000\000\000\000\
\008\000\013\000\005\000\000\000\000\000\009\000\010\000"

let yydgoto = "\002\000\
\004\000\005\000\008\000\012\000\019\000\017\000"

let yysindex = "\005\000\
\008\255\000\000\000\000\000\000\001\000\000\255\000\000\000\000\
\000\000\013\255\000\000\004\255\000\000\015\255\012\255\014\255\
\000\000\000\000\000\000\015\255\015\255\000\000\000\000"

let yyrindex = "\000\000\
\002\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\003\000\000\000\000\000\
\000\000\000\000\000\000\011\255\011\255\000\000\000\000"

let yygindex = "\000\000\
\000\000\000\000\000\000\000\000\249\255\000\000"

let yytablesize = 263
let yytable = "\009\000\
\007\000\003\000\012\000\013\000\014\000\001\000\010\000\003\000\
\015\000\016\000\012\000\012\000\022\000\023\000\011\000\012\000\
\012\000\018\000\020\000\000\000\021\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\006\000\003\000\012\000"

let yycheck = "\000\001\
\000\000\000\000\000\000\000\001\001\001\001\000\007\001\000\001\
\005\001\006\001\000\001\001\001\020\000\021\000\002\001\005\001\
\006\001\003\001\007\001\255\255\007\001\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\
\255\255\255\255\255\255\255\255\004\001\004\001\004\001"

let yynames_const = "\
  CLOSING_BRACKET\000\
  OPENING_BRACKET\000\
  SEMICOLON\000\
  FILE\000\
  NAME\000\
  REGEXP\000\
  EOF\000\
  "

let yynames_block = "\
  STRING\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'file_decl_list) in
    Obj.repr(
# 49 "src/syntax/excludeParser.mly"
                                 ( List.rev _1 )
# 169 "src/syntax/excludeParser.ml"
               : Exclude.file list))
; (fun __caml_parser_env ->
    Obj.repr(
# 50 "src/syntax/excludeParser.mly"
                                 ( fail Invalid_file_contents )
# 175 "src/syntax/excludeParser.ml"
               : Exclude.file list))
; (fun __caml_parser_env ->
    Obj.repr(
# 52 "src/syntax/excludeParser.mly"
                                 ( [] )
# 181 "src/syntax/excludeParser.ml"
               : 'file_decl_list))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'file_decl_list) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'file_decl) in
    Obj.repr(
# 53 "src/syntax/excludeParser.mly"
                                 ( _2 :: _1 )
# 189 "src/syntax/excludeParser.ml"
               : 'file_decl_list))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 4 : string) in
    let _4 = (Parsing.peek_val __caml_parser_env 2 : 'exclusion_list) in
    let _6 = (Parsing.peek_val __caml_parser_env 0 : 'separator_opt) in
    Obj.repr(
# 56 "src/syntax/excludeParser.mly"
                                 ( { Exclude.path = _2;
                                     Exclude.exclusions = List.rev _4; } )
# 199 "src/syntax/excludeParser.ml"
               : 'file_decl))
; (fun __caml_parser_env ->
    Obj.repr(
# 58 "src/syntax/excludeParser.mly"
                                 ( fail Invalid_file_declaration )
# 205 "src/syntax/excludeParser.ml"
               : 'file_decl))
; (fun __caml_parser_env ->
    Obj.repr(
# 60 "src/syntax/excludeParser.mly"
                                 ( [] )
# 211 "src/syntax/excludeParser.ml"
               : 'exclusion_list))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'exclusion_list) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'exclusion) in
    Obj.repr(
# 61 "src/syntax/excludeParser.mly"
                                 ( _2 :: _1 )
# 219 "src/syntax/excludeParser.ml"
               : 'exclusion_list))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'separator_opt) in
    Obj.repr(
# 64 "src/syntax/excludeParser.mly"
                                 ( Exclude.Name _2 )
# 227 "src/syntax/excludeParser.ml"
               : 'exclusion))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'separator_opt) in
    Obj.repr(
# 65 "src/syntax/excludeParser.mly"
                                 ( try
                                     Exclude.Regexp (Str.regexp _2)
                                   with _ -> fail (Invalid_regular_expression _2) )
# 237 "src/syntax/excludeParser.ml"
               : 'exclusion))
; (fun __caml_parser_env ->
    Obj.repr(
# 68 "src/syntax/excludeParser.mly"
                                 ( fail Invalid_exclusion )
# 243 "src/syntax/excludeParser.ml"
               : 'exclusion))
; (fun __caml_parser_env ->
    Obj.repr(
# 70 "src/syntax/excludeParser.mly"
                                 ( )
# 249 "src/syntax/excludeParser.ml"
               : 'separator_opt))
; (fun __caml_parser_env ->
    Obj.repr(
# 71 "src/syntax/excludeParser.mly"
                                 ( )
# 255 "src/syntax/excludeParser.ml"
               : 'separator_opt))
(* Entry file *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let file (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf : Exclude.file list)
;;
