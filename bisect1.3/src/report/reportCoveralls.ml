(*
 * This file is part of Bisect.
 * Copyright (C) 2008-2012 Xavier Clerc.
 *
 * Bisect is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Bisect is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open ReportUtils

let escape_line line offset points =
  let buff = Buffer.create (String.length line) in
  let ofs = ref offset in
  let pts = ref points in
  let marker n =
    Buffer.add_string buff "(*[";
    Buffer.add_string buff (string_of_int n);
    Buffer.add_string buff "]*)" in
  let marker_if_any () =
    match !pts with
    | (o, n) :: tl when o = !ofs ->
        marker n;
        pts := tl
    | _ -> () in
  String.iter
    (fun ch ->
      marker_if_any ();
      (match ch with
      | '\"' -> Buffer.add_string buff "\\\""
      | '\\' -> Buffer.add_string buff "\\\\"
      | _ -> Buffer.add_char buff ch);
      incr ofs)
    line;
  List.iter (fun (_, n) -> marker n) !pts;
  Buffer.contents buff

let source_and_coverage in_file resolver visited =
  let cmp_content = Common.read_points (resolver in_file) in
  let len = Array.length visited in
  let stats = ReportStat.make () in
  let pts = ref (List.map
                   (fun p ->
                     let nb =
                       if p.Common.identifier < len then
                         visited.(p.Common.identifier)
                       else
                         0 in
                     ReportStat.update stats p.Common.kind (nb > 0);
                     (p.Common.offset, nb))
                   cmp_content) in
  let in_channel = open_in in_file in
  let source = Buffer.create 512 in
  let coverage = Buffer.create 512 in
  let line_no = ref 0 in
  (try
    (try
      while true do
        incr line_no;
        let start_ofs = pos_in in_channel in
        let line = input_line in_channel in
        let end_ofs = pos_in in_channel in
        let before, after = split (fun (o, _) -> o < end_ofs) !pts in
        let line' = escape_line line start_ofs before in
        let min_visits, visited, unvisited =
          List.fold_left
            (fun (m, v, u) (_, nb) ->
              (min m nb, (v || (nb > 0)), (u || (nb = 0))))
            (max_int, false, false)
            before in
        let line_coverage = match visited, unvisited with
        | false, false -> "null"
        | false, true -> "0"
        | true, false -> string_of_int min_visits
        | true, true -> "0" in
        if !line_no > 1 then begin
          Buffer.add_string source "\\n";
          Buffer.add_string coverage ", ";
        end;
        Buffer.add_string source line';
        Buffer.add_string coverage line_coverage;
        pts := after
      done
    with End_of_file -> ());

  with e ->
    close_in_noerr in_channel;
    raise e);
  close_in_noerr in_channel;
  Buffer.contents source, Buffer.contents coverage

let output properties file resolver data =
  let files = Hashtbl.fold
      (fun in_file visited acc ->
        let source, coverage = source_and_coverage in_file resolver visited in
        (in_file, source, coverage) :: acc)
      data
      [] in
  Common.try_out_channel
    false
    file
    (fun channel ->
      output_strings ["{"] [] channel;
      List.iter
        (fun (key, value) ->
          output_strings
            ["  \"$(key)\": \"$(value)\","]
            ["key", String.escaped key ;
             "value", String.escaped value ]
          channel)
        properties;
      output_strings ["  \"source_files\": ["] [] channel;
      let first = ref true in
      List.iter
        (fun (in_file, source, coverage) ->
          if !first then
            first := false
          else
            output_strings ["    ,"] [] channel;
          output_strings
            ["    {" ;
             "      \"name\": \"$(file)\"," ;
             "      \"source\": \"$(source)\"," ;
             "      \"coverage\": [$(coverage)]" ;
             "    }"]
            ["file", String.escaped in_file ;
             "source", source ;
             "coverage", coverage ]
            channel)
        files;
      output_strings ["  ]"] [] channel;
      output_strings ["}"] [] channel)
